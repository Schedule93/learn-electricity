### [return main page](../../README.md)

### Topics. Basic Electricity :
* Topic 1. Что такое ток и наприжение
* Topic 2. ?
* Topic 3. ?
* Topic 4. ?
* Topic 5. ?
* Topic 6. ?
* Topic 7. ?
* Topic 8. ?
* Topic 9. ?
* Topic 10. ?

### Notices :
**note 1** - В электрике есть две основные проблемы :
1. Контакт где его не должно быть
1. Обрыв, где его не должно быть 

### YouTube Video :
* [Канал - Радиолюбитель - PlayList: Уроки радиоэлектроники - Урок №1. Напряжение и ток. В чем разница?](https://www.youtube.com/watch?v=DmGBdgME-7k&list=RDCMUC3KvOdEZ3qHwtL2jM30qY0g&start_radio=1&t=4s&ab_channel=%D0%A0%D0%B0%D0%B4%D0%B8%D0%BE%D0%BB%D1%8E%D0%B1%D0%B8%D1%82%D0%B5%D0%BB%D1%8C)

### Extern links :
* link 1
* link 2
