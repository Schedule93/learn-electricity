![img](img/anime/sasuke_wallpaper_2.png)

### Уроки электричество с нуля.
* [Основы электричество](md/basic-electricity/basic_electricity.md)
* [Devices](md/devices/DEVICES.md)
* [Tasks](md/TASKS.md)
* [Questions](md/QUESTIONS.md)

### Advanced Topics :
* [Physics](md/physics/PHYSICS.md)
* [Panouri Solarare](md/panouri-solare/panouri_solare.md)
* [Arduino](md/arduino/ARDUINO.md)
* [Умный дом](md/smart-home/smart_home.md)
* [Печатные платы](md/printed_circut_boards/печатные_платы.md) 
* [Радиоэлектроника](md/radio electronics/RADIO_ELECTRONICS.md)
* [Автоэлектрик](md/auto-electrician/auto_electrician.md)

### Youtube Video :
* [ElectronicsClub](https://www.youtube.com/watch?v=2eWhTQYIQYs)
* [Электроника для начинающих](https://www.youtube.com/watch?v=9IKzf6Medww&list=PL8uwGGI-Cxq7_82j2kLih6bspk4DkOMec)

### Extern links :
* [ТЕРМИН: ЦЕПЬ СИГНАЛЬНАЯ (ЭЛЕКТРИЧЕСКАЯ)](https://www.lcard.ru/lexicon/signal_line)
